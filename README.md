Instrukcja uruchomienia Swagger'a:

1. Przebudować projekt za pomocą Maven'a: 
a)W folderze "Lifecycle" kliknąć na clean i install 
b)Uruchomić "Run Maven Build"
   
2. Po poprawnym wykonaniu poprzedniego punktu uruchomić projekt Spring Boot
3. Jeżeli uruchomienie projektu nie zakończy się błędem to otworzyć przeglądarkę internetową
i wpisać adres: 
http://localhost:8080/swagger-ui.html
lub
http://localhost:8080/swagger-ui.html#/ 

UWAGA: Domyślnie dla usługi został ustawiony port 8080. 
Port dla wywołania usługi można zmieniać w pliku konfiguracyjnym projektu: "application.properties"