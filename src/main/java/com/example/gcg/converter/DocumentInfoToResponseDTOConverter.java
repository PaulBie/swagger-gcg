package com.example.gcg.converter;

import com.example.gcg.controller.DemoRestController;
import com.example.gcg.dto.DocumentsJsonDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class DocumentInfoToResponseDTOConverter implements Converter<List<DemoRestController.DocumentInfo>, DocumentsJsonDTO>  {

    @Override
    public DocumentsJsonDTO convert(List<DemoRestController.DocumentInfo> documentInfos) {
        DocumentsJsonDTO documentsJsonDTO = new DocumentsJsonDTO();

        List<Map<String, Object>> outputListToJson = documentInfos.stream().map(this::convert).collect(Collectors.toList());

        documentsJsonDTO.setGenerated(outputListToJson);

        return documentsJsonDTO;
    }

    private Map<String, Object> convert(DemoRestController.DocumentInfo src) {
        Map<String, Object> params = new HashMap<>();
        params.put(src.getId(), src.getParentNodeId());

        return params;
    }

}
