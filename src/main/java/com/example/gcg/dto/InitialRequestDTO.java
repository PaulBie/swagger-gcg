package com.example.gcg.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "wfd",
        "processId",
        "documents"
})
@Data
public class InitialRequestDTO {

    @JsonProperty("wfd")
    private String wfd;
    @JsonProperty("processId")
    private String processId;
    @JsonProperty("documents")
    private List<Document> documents = new ArrayList<Document>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "id",
            "templateData",
            "repository",
            "signature"
    })
    @Data
    public static final class Document{
        @JsonProperty("id")
        private String id;
        @JsonProperty("templateData")
        private TemplateData templateData;
        @JsonProperty("repository")
        private Repository repository;
        @JsonProperty("signature")
        private Signature signature;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    }


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "parentNodeId",
            "aspects",
            "properties"
    })
    @Data
    public static final class Repository{
        @JsonProperty("parentNodeId")
        private String parentNodeId;
        @JsonProperty("aspects")
        private List<String> aspects = new ArrayList<>();
        @JsonProperty("properties")
        private Properties properties;
        @JsonIgnore
        private Map<String, Object> Additionalproperties = new HashMap<String, Object>();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "worm",
            "signprops",
            "stampprops"
    })
    @Data
    public static final class Signature {

        @JsonProperty("worm")
        private Boolean worm;
        @JsonProperty("signprops")
        private Signprops signprops;
        @JsonProperty("stampprops")
        private Stampprops stampprops;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "prefix:aspectName"
    })
    @Data
    public static final class Properties{
        @JsonProperty("prefix:aspectName")
        private PrefixAspectName prefixAspectName;
        @JsonIgnore
        private Map<PrefixAspectName, Object> additionalPrefixAspectName= new HashMap<PrefixAspectName, Object>();

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "signerReason"
    })
    @Data
    public static final class ExtraParameters {

        @JsonProperty("signerReason")
        private String signerReason;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "aspect-property",
            "aspect-property-2"
    })
    @Data
    public static final class PrefixAspectName{
        @JsonProperty("aspect-property")
        private String aspectProperty;
        @JsonProperty("aspect-property-2")
        private String aspectProperty2;
        @JsonIgnore
        private Map<String, Object> additionalAspectProperty = new HashMap<String, Object>();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "documentTypeId",
            "visualization:id",
            "visualization:mode",
            "visualization:text",
            "visualization:font",
            "extraParameters"
    })
    @Data
    public static final class Signprops {

        @JsonProperty("documentTypeId")
        private String documentTypeId;
        @JsonProperty("visualization:id")
        private String visualizationId;
        @JsonProperty("visualization:mode")
        private String visualizationMode;
        @JsonProperty("visualization:text")
        private String visualizationText;
        @JsonProperty("visualization:font")
        private VisualizationFont visualizationFont;
        @JsonProperty("extraParameters")
        private ExtraParameters extraParameters;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    }



    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({

    })
    public static final class Stampprops {

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({

    })
    public static final class TemplateData {

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "name",
            "size"
    })
    @Data
    public static final class VisualizationFont {

        @JsonProperty("name")
        private String name;
        @JsonProperty("size")
        private Integer size;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    }

}
