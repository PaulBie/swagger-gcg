package com.example.gcg.controller;


import com.example.gcg.converter.DocumentInfoToResponseDTOConverter;
import com.example.gcg.dto.DocumentsJsonDTO;
import com.example.gcg.dto.InitialRequestDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/v1" )
public class DemoRestController {

    private List<DocumentInfo> documentInfoList = new ArrayList<>();

    private DocumentInfoToResponseDTOConverter documentInfoToResponseDTOConverter;

    public DemoRestController(DocumentInfoToResponseDTOConverter documentInfoToResponseDTOConverter){
        this.documentInfoToResponseDTOConverter = documentInfoToResponseDTOConverter;

    }

    @PostMapping("/documents")
    @ResponseBody
    public ResponseEntity<DocumentsJsonDTO> initPost(@RequestBody InitialRequestDTO initialRequestDTO,
                                         HttpServletRequest httpServletRequest,
                                         HttpServletResponse httpServletResponse) {

        if(initialRequestDTO != null){
            initialRequestDTO.getWfd();
            initialRequestDTO.getProcessId();
            initialRequestDTO.getDocuments();
        }

        List<DocumentInfo> documentInfoList = initialRequestDTO
                .getDocuments()
                .stream()
                .map(this::mapperList)
                .collect(Collectors.toList());

        httpServletResponse.getStatus();

        DocumentsJsonDTO documents = documentInfoToResponseDTOConverter.convert(documentInfoList);

        return new ResponseEntity<>(documents, HttpStatus.OK);
    }

    private DocumentInfo mapperList(InitialRequestDTO.Document document){
        return new DocumentInfo(document.getId(), document.getRepository().getParentNodeId());
    }

    @Data
    @AllArgsConstructor
    public class DocumentInfo{
        private String id;
        private String parentNodeId;
    }
}
